

CREATE DATABASE IF NOT EXISTS twitter;

use twitter;
CREATE TABLE IF NOT EXISTS accounts
(
    acc_id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
    acc_name VARCHAR(200)
) ENGINE=InnoDB;

CREATE TABLE IF NOT EXISTS hashtags
(
    htag_id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
    htag_name VARCHAR(200),
    positive INT,
    negative INT,
    neutral INT
) ENGINE=InnoDB;


CREATE TABLE IF NOT EXISTS tweets
(
    tweet_id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
    acc_id INT NOT NULL,
    tweet VARCHAR(512) CHARACTER SET utf8mb4,
    twitter_tweet_id VARCHAR(50),
    last_searched_id VARCHAR(50),
    date DATETIME,
    positive INT,
    negative INT,
    neutral INT,
    language VARCHAR(20),
    FOREIGN KEY(acc_ID) references accounts(acc_ID)
) ENGINE=InnoDB;


CREATE TABLE IF NOT EXISTS responses
(
    response_id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
    tweet_id INT,
    htag_id INT,
    twitter_tweet_id VARCHAR(50),
    date DATETIME,
    tweet VARCHAR(512) CHARACTER SET utf8mb4,
    sentiment VARCHAR(20),
    FOREIGN KEY(tweet_id) references tweets(tweet_id),
    FOREIGN KEY(htag_id) references hashtags(htag_id)
) ENGINE=InnoDB;