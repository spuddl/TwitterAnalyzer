import re
from textblob import TextBlob
import logging

## Easy implementation: https://www.geeksforgeeks.org/twitter-sentiment-analysis-using-python/
## https://textblob.readthedocs.io/en/dev/
# https://github.com/ipublia/sentiment-analysis
# http://www.laurentluce.com/posts/twitter-sentiment-analysis-using-python-and-nltk/

def clean_tweet(tweet):
    '''
    Utility function to clean tweet text by removing links, and hashtag and @ symbols
    using simple regex statements.
    '''
    return ' '.join(re.sub("([^0-9A-Za-z.,;:!? \t])|(\w+:\/\/\S+)", " ", tweet).split())


def get_analysis(text):
    positive =0
    negative = 0
    neutral = 0
    text = clean_tweet(text)

    analysis = TextBlob(text)
    log = logging.getLogger("log_analysis")
    #sentiment indicates positive negative or neutral
    if analysis.sentiment.polarity > 0.1:
        positive = 1
        log.debug(u" # POSITIVE: \"{}\" ".format(text))
    elif analysis.sentiment.polarity < -0.1:
        negative = 1
        log.debug(u" # NEGATIVE: \"{}\"".format(text))
    else:
        neutral = 1
        log.debug(u" # NEUTRAL: \"{}\" ".format(text))

    return (positive,negative,neutral)