
import MySQLdb

import logging
class Database(object):
    ## SQL Statements
    _sql_add_person = u"INSERT INTO accounts" \
                  u"(acc_name)" \
                  u"VALUES (%s) "

    _sql_add_hashtag = u"INSERT INTO hashtags" \
                      u"(htag_name,positive,negative,neutral)" \
                      u"VALUES (%s, %s, %s, %s) "

    _sql_add_tweet = u"INSERT INTO tweets" \
                  u"(acc_id, tweet, date, positive, negative, neutral, language, twitter_tweet_id, last_searched_id)" \
              u"VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s)"

    _sql_get_person = u"SELECT * from accounts WHERE acc_name=%s"

    _sql_get_hashtag = u"SELECT * from hashtags WHERE htag_name=%s"

    _sql_add_response = u"INSERT INTO responses" \
                        u"(tweet_id,htag_id, twitter_tweet_id,date,tweet,sentiment)" \
                        u"VALUES(%s,%s,%s,%s,%s,%s)"

    def __init__(self,host,user,user_pwd,db):
        self.log = logging.getLogger("log_main_stream")
        try:
            self.db_con = MySQLdb.connect(host,user,user_pwd,db,use_unicode=True,charset='utf8')

            self.cursor = self.db_con.cursor()

            self.cursor.execute('SET NAMES utf8mb4;')
            self.cursor.execute('SET CHARACTER SET utf8mb4;')
            self.cursor.execute('SET character_set_connection=utf8mb4;')

        except MySQLdb.Error,e:
            self.log.exception("MySQL Error in connection. Is MySQL Server installed and running?" + str(e))
            exit(1)

    def insert(self,query,options=None):
        try:
            self.cursor.execute(query,options)
            self.db_con.commit()
        except MySQLdb.Error,e:
            self.log.error(u"There has been an error, Check the query: {} with values {}".format(query,options))
            self.db_con.rollback()

    def query(self,query,options=None):
        try:
            cursor = self.db_con.cursor(MySQLdb.cursors.DictCursor)
            cursor.execute(query,options)
            return cursor.fetchall()
        except MySQLdb.Error:
            self.log.error(u"There has been an error, Check the query: {} with values {}".format(query,options))


    def query_one(self,query,options=None):
        try:
            cursor = self.db_con.cursor(MySQLdb.cursors.DictCursor)
            cursor.execute(query,options)
            return cursor.fetchone()
        except MySQLdb.Error:
            self.log.error(u"There has been an error, Check the query: {} with values {}".format(query,options))

    def __del__(self):
        self.cursor.close()
        self.db_con.close()


    def get_all_person_names(self):

        res = self.query(u"SELECT * FROM accounts")
        person_list = []

        for item in res:
            person = item['acc_name']
            if person != "":
                person_list.append(person)

        return person_list

    def get_all_hashtags(self):

        res = self.query(u"SELECT * FROM hashtags")
        hashtag_list = []

        for item in res:
            hashtag = item['htag_name']
            if hashtag != "":
                hashtag_list.append(hashtag)

        return hashtag_list

    # Adds new Person in case it doesnt exist already
    def add_person(self,name):
        res = self.query(self._sql_get_person,[name])

        if not res:
            self.insert(self._sql_add_person,[name])


    def add_hashtag(self,name,pos,neg,neut):
        """
        Adds a new hashtag in case it does not exit or upates it in case it exists
        :param name:
        :param pos:
        :param neg:
        :param neut:
        :return:
        """
        res = self.query(self._sql_get_hashtag,[name])

        update_query = u"UPDATE hashtags SET positive=%s, negative=%s, neutral=%s" \
                       u" WHERE htag_name=%s "
        if not res:
            self.insert(self._sql_add_hashtag,[name,pos,neg,neut])
        else:
            self.insert(update_query,[pos,neg,neut,name])

    def get_tweet(self,tweet_id):
        qry = u"SELECT * FROM tweets where twitter_tweet_id = %s"
        res = self.query(qry,[tweet_id])
        return res


    def get_hashtag(self,hashtag):
        hashtag_elem = self.query(self._sql_get_hashtag,[hashtag])

        return hashtag_elem


    def get_latest_tweets(self,acc_name,max=10):
        res = self.query(self._sql_get_person,[acc_name])
        list_ids = []
        qry = u"SELECT * FROM tweets WHERE acc_id = %s LIMIT %s"

        if res:
            tweets = self.query(qry,[res[0]['acc_id'],max])

            return tweets

        else:
            return []

    def add_response(self,response_twitter_id,date,text,sentiment,tweet_id=None,htag_id=None):
        """
        Adds a response to the database
        :param tweet_id:
        :param date:
        :param sentiment:
        :return:
        """
        self.insert(self._sql_add_response,[tweet_id,htag_id,response_twitter_id,date,text,sentiment])


    def add_tweet(self,date, positive, negative, neutral,language, tweet_id, last_searched_id, acc_name,tweet=""):
        """
        Adds a tweet to the database, if it already exists, will be updated
        :param date:
        :param positive:
        :param negative:
        :param neutral
        :param language:
        :param tweet_id:
        :param last_searched_id:
        :param acc_name:
        :param hash_tag:
        :param tweet:
        :return: void
        """
        res = self.query(self._sql_get_person,[acc_name])

        #Result is tuple of dictionaries, but in that case, first entry is always right
        acc_id = res[0]['acc_id']

        #one entry per day and tweet, in case of no tweets one per day (e.g. hahstags)
        ##In case entry for the date already exits, update (delete and then add)

        tweet_res = self.query(u"SELECT * FROM tweets WHERE acc_id=%s and twitter_tweet_id=%s",[acc_id,tweet_id])

        update_query = u"UPDATE tweets SET tweet=%s,positive=%s, negative=%s, neutral=%s, language=%s, last_searched_id=%s" \
                    u" WHERE acc_id=%s and twitter_tweet_id=%s"

        if tweet_res:
            self.insert(update_query,(tweet,positive,negative,neutral,language,last_searched_id,acc_id,tweet_id))
        else:
            self.insert(self._sql_add_tweet,(acc_id,tweet,date,positive,negative,neutral,language,tweet_id,last_searched_id))