###### Twitter Semantical Analyser
###### Written by spuddl
import os
import sys
import logging
import argparse

def handle_args(dir_path):
    ##Set logging level from commandline
    parser = argparse.ArgumentParser()
    parser.add_argument(
        '-d', '--debug',
        help="print debug statements to logfile",
        action="store_const", dest="loglevel", const=logging.DEBUG,
        default=logging.WARNING,
    )
    parser.add_argument(
        '-v', '--verbose',
        help="verbose output to logfile",
        action="store_const", dest="loglevel", const=logging.INFO, default=logging.WARNING
    )

    parser.add_argument('-s' '--sentiment', help ="Print detailed sentiment information in log file" ,
        action="store_const", dest="loglevel_sent", const=logging.DEBUG,default=logging.WARNING)


    args = parser.parse_args()

    log = logging.getLogger("log_main_stream")

    logging.basicConfig(
        format="%(asctime)-15s [%(threadName)-12.12s] [%(levelname)-5.5s]  %(message)s")
    log.addHandler(logging.FileHandler(os.path.join(dir_path,"start_analyser.log")))
    log.addHandler(logging.StreamHandler(sys.stdout))
    log.setLevel(args.loglevel)

    log_analysis = logging.getLogger("log_analysis")
    log_analysis.addHandler(logging.FileHandler(os.path.join(dir_path,"sentiment_analysis.log")))
    log_analysis.addHandler(logging.StreamHandler(sys.stdout))
    log_analysis.setLevel(args.loglevel_sent)
    ## Mentions directly at person,hashtag etc.. save only the analysis
    ## Replies to tweet, also save original tweet


def main():
    import ConfigParser
    import tweepy
    from database import Database

    from stream_handler import OwnStreamListener

    from pid import PidFile,PidFileError
    import time

    ## Get Configuration data from config file
    dir_path = os.path.dirname(os.path.realpath(__file__))
    # get root dir
    dir_path , _ = os.path.split(dir_path)
    dir_path , _ = os.path.split(dir_path)

    config = ConfigParser.ConfigParser()
    config.read(os.path.join(dir_path,"config.ini"))

    handle_args(dir_path)

    ## Create Authentication to Twitter API
    auth = tweepy.OAuthHandler(config.get('Twitter','CONSUMER_KEY'), config.get('Twitter','CONSUMER_SECRET'))
    auth.set_access_token(config.get('Twitter','ACCESS_KEY'), config.get('Twitter','ACCESS_SECRET'))
    twitter_api = tweepy.API(auth,wait_on_rate_limit=True,wait_on_rate_limit_notify=True)

    persons = config.get("DEFAULT","PERSONS").split(",")
    hashtaglist = config.get("DEFAULT","HASHTAGS").split(",")





    #Create DataBase object
    database = Database(config.get("MySQL","HOST"),config.get("MySQL","USER_NAME"),config.get("MySQL","USER_PWD"), config.get("MySQL","DB_NAME"))

    print("Start Analyser...")
    print("Adding users and hashtags to the Database")

    perslist =[]

    #Add all users to DB
    for person in persons:
        try:
            user = twitter_api.get_user(screen_name=person)
            perslist.append(user.id_str)
            database.add_person(person)
        except tweepy.TweepError:
            logging.info("user {} not found".format(person))

    for hashtag in hashtaglist:
        # Add new hashtag with default entries only if it does not exist
        if not database.get_hashtag(hashtag):
            database.add_hashtag(hashtag,0,0,0)


    #### Get some initial start tweets for all the users ####
    """
    print("Get the latest tweets of all the persons....")

    for person in persons:
        counter = 0
        for status in tweepy.Cursor(twitter_api.user_timeline,id=person,tweet_mode='extended').items():
            date = status.created_at.date()

            text = status.full_text

            #only add in case not in database
            if not database.get_tweet(status.id_str):
                database.add_tweet(date, 0, 0, 0, "en", status.id_str, status.id_str, acc_name=person, tweet=text)
            counter = counter +1

            if counter > 20:
                break
    """

    # Register the Streaming API with all the persons from the database

    stream_listener = OwnStreamListener(database,perslist,hashtaglist,twitter_api)
    stream = tweepy.Stream(auth = twitter_api.auth, listener=stream_listener)

    logging.info(u"Follow persons: {}    \n     and Hashtags: {}".format(str(persons),str(hashtaglist)))

    # Filter for all the relevant hashtags and persons

    print("Starting to listen")

    try:
        with PidFile(piddir=dir_path):

            cont = True
            wait_count = 10

            while cont:
                try:
                    stream.filter(follow=perslist,track=hashtaglist)

                #TweepError in case of an error in the connection to Twitter API
                except tweepy.TweepError:
                    logging.exception("Tweep error occured,sleep for {}s".format(wait_count))
                    time.sleep(wait_count)

                #Catch general exception so program won't terminate.
                #Should not happen, therefore exceptions are written to file for further handling
                except:
                    logging.exception("General exception occured, sleep for {}s".format(wait_count))
                    time.sleep(wait_count)

    except PidFileError:
        logging.error("Could not start. pid File already existing.")

if __name__ == "__main__":
    main()
