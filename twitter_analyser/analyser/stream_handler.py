import tweepy
import logging
from Analyser import get_analysis

max_historic_tweets = 50

class OwnStreamListener(tweepy.StreamListener):

    def __init__(self, database,perslist,hashtaglist,twitter_api):
        super(OwnStreamListener, self).__init__()
        self.database = database
        self.perslist = perslist
        self.hashtaglist = hashtaglist
        self.log = logging.getLogger("log_main_stream")
        self.twitter_api = twitter_api

    def _calc_sentiment(self,tweet,text):
        """
        updates the tweet object in parameter
        :param tweet:
        :param text: text to analyse
        :return: sentiment value
        """
        (pos, neg, neut) = get_analysis(text)
        # Add the calculated values to the previous ones
        tweet['positive'] = tweet['positive'] + pos
        tweet['negative'] = tweet['negative'] + neg
        tweet['neutral'] = tweet['neutral'] + neut

        sentiment = "positive"
        # Add to DB for debugging purposes:
        if neut == 1:
            sentiment = "neutral"
        elif neg == 1:
            sentiment = "negative"

        return sentiment

    def on_status(self,status):

        try:

            """
            Handle the status
            :param status:
            :return:
            """

            #Don't count retweets
            if status.retweeted:
                return

             ##Some tweet variables
            date = status.created_at.date()
            text = status.text
            if hasattr(status,'extended_tweet'):
                text = status.extended_tweet['full_text']

            id = status.id_str
            lang = "en"  # TODO support for multiple languages (detect languages with textblob google)
            author = status.user.screen_name

            self.log.debug(u"Incoming status of author {} and text: {}".format(author,text))

            perslist = self.database.get_all_person_names()

            #Hashtag

            hashtags = status.entities.get('hashtags')
            tags_in_status = []
            for tag in hashtags:
                tags_in_status.append(tag['text'])

            #check if one of the hashtags is one which is searched for
            for tag in tags_in_status:
                for other_tag in self.hashtaglist:
                    if tag == other_tag:
                        # Get the hashtag tweet element

                        hashtag_elem = self.database.get_hashtag(tag)[0]
                        # only one hashtag element
                        sentiment = self._calc_sentiment(hashtag_elem,text)

                        self.database.add_hashtag(tag,hashtag_elem['positive'], hashtag_elem['negative'],hashtag_elem['neutral'])

                        self.database.add_response(htag_id= hashtag_elem['htag_id'], response_twitter_id= status.id_str,
                                                   date= (status.created_at.date()), text = text, sentiment= sentiment)


            #Tweet of somebeody in perslist
            if status.user.screen_name in perslist:
                self.log.info(u"   -- Tweet from {}:  {}".format(author,text))
                #Add tweet with Default values
                self.database.add_tweet(date, 0, 0, 0, lang, id, id, acc_name=author, tweet=text)

            #Reply to one of the persons in the list
            else:

                reply_id = status.in_reply_to_status_id_str
                tweet = self.database.get_tweet(reply_id)
                pers = status.in_reply_to_screen_name

                if pers is not None and reply_id is not None:

                    #case insensitive
                    if str(pers).lower() not in (name.lower() for name in perslist):
                        self.log.info("Error: {} is not in perslist ".format(pers))
                        return

                    #tweet does not exist in database
                    if not tweet:
                        self.log.debug(u"Found a NEW reply for tweet that doesnt exist. Adding tweet")

                        tweet_obj = self.twitter_api.get_status(reply_id)
                        tweet_date = tweet_obj.created_at.date()
                        tweet_text = tweet_obj.text
                        if hasattr(tweet_obj,'extended_tweet'):
                            tweet_text = tweet_obj.extended_tweet['full_text']

                        self.database.add_tweet(tweet_date,0,0,0,lang,reply_id,reply_id,acc_name=tweet_obj.user.screen_name,tweet= tweet_text)
                        tweet = self.database.get_tweet(reply_id)

                    if tweet:
                        tweet = tweet[0]
                        self.log.debug(u"Found a NEW reply for tweet {} \n --> Reply: {}".format(tweet['tweet'],text))
                        sentiment= self._calc_sentiment(tweet,text)

                        self.database.add_tweet(tweet['date'], tweet['positive'], tweet['negative'], tweet['neutral'], tweet['language'],
                                            tweet['twitter_tweet_id'], tweet['last_searched_id'], acc_name=pers, tweet=tweet['tweet'])

                        self.database.add_response(tweet_id=tweet['tweet_id'], response_twitter_id=status.id_str,
                                               date= (status.created_at.date()), text= text, sentiment= sentiment)


        except UnicodeDecodeError:
            self.log.exception("Unicode Decode Error occured!")


    def on_error(self, status_code):
        self.log.exception("Error occured during Streaming - Status Code: {}".format(status_code))
