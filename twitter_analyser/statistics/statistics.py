
#Access to the database of the analyser
import logging
import os

import argparse
import sys

from twitter_analyser.analyser.database import Database
import ConfigParser
import matplotlib.pyplot as plot
import numpy
import datetime


#https://pythonspot.com/matplotlib-bar-chart/

class Statistics(Database):

    _sql_get_acc_id = u"SELECT acc_id from accounts where acc_name=%s"
    _sql_get_hashtag_id =u"SELECT htag_id from hashtags where htag_name=%s"
    _sql_get_acc_name =u"SELECT * from accounts where acc_id=%s"
    _sql_number_tweets = u"SELECT count(*) from tweets where acc_id=%s"
    _sql_get_tweets_date = u"SELECT * from tweets where acc_id=%s and date=%s"
    _sql_get_tweets = u"SELECT * from tweets where acc_id=%s"
    _sql_get_responses = u"SELECT * from responses where tweet_id=%s"
    _sql_get_responses_date = u"SELECT * from responses where tweet_id=%s and date=%s"
    _sql_get_responses_htag = u"SELECT * from responses where htag_id=%s"
    _sql_get_responses_htag_date = u"SELECT * from responses where htag_id=%s and date=%s"

    def __init__(self,host,user,user_pwd,db):

        dir_path = os.path.dirname(os.path.realpath(__file__))
        dir_path , _ = os.path.split(dir_path)
        dir_path , _ = os.path.split(dir_path)

        self.path = os.path.join(dir_path,"figures")

        super(Statistics,self).__init__(host,user,user_pwd,db)

    #TODO overall statistics file for all persons, like on this tutorial
    #http://emptypipes.org/2013/11/09/matplotlib-multicategory-barchart/
    def get_overall_statistics(self,date_back=0):
        """
        Statistic for all the persons:
        * total number of tweets,
        * average responses per tweet,
        * average positive to negative ratio
        :return:
        """

        persons = self.get_all_person_names()
        hashtags = self.get_all_hashtags()

        today = datetime.date.today()

        for person in persons:
            self.log.info("Plotting figures for account {}".format(person))
            self.get_statistics_by_person(person)
            self.get_statistics_by_person(person, date=today)
            for i in range(date_back):
                self.get_statistics_by_person(person, date=today-datetime.timedelta(days=i))

        for hashtag in hashtags:
            self.log.info("Plotting figures for hashtag {}".format(hashtag))
            self.get_statistics_by_hashtag(hashtag)
            self.get_statistics_by_hashtag(hashtag,date=today)
            for i in range(date_back):
                self.get_statistics_by_hashtag(hashtag,date=today-datetime.timedelta(days=i))

    def print_plot(self,objects,values,label,person):
        y_pos = numpy.arange(len(objects))

        plot.clf() #clears the figure
        plot.bar(y_pos,values,align='center',alpha=0.5)
        plot.xticks(y_pos,objects)
        plot.title(label)
        finalpath = os.path.join(self.path,person)
        if not os.path.exists(finalpath):
            os.makedirs(finalpath)
        plot.savefig(os.path.join(finalpath,label+".png"),facecolor='w')

    def get_statistics_by_person(self,person,date=None):
        """
        * Average responses per tweet
        * Average Positive to negative ratio
        * Total Number of tweets
        * Total Number of responses to the tweets
        :return: [total_responses,pos_total,neg_total,number_tweets,avg_responses,pos_avg,neg_avg,avg_ratio]
        """

        res = self.query_one(self._sql_get_acc_id,[person])
        if res:
            id = res['acc_id']

            res = self.query_one(self._sql_number_tweets,[id])
            number_tweets = res['count(*)']
            if date:
                list_of_tweets = self.query(self._sql_get_tweets_date,[id,date])
                number_tweets = len(list_of_tweets)
            else:
                list_of_tweets = self.get_latest_tweets(person,number_tweets)

            avg_ratio =0.0
            avg_responses = 0.0
            pos_total = 0.0
            neg_total = 0.0
            total_responses = 0.0
            pos_avg = 0.0
            neg_avg = 0.0
            for tweet in list_of_tweets:
                responses = self.get_responses_for_tweet(tweet['tweet_id'])
                pos_total = pos_total + tweet['positive']
                neg_total = neg_total + tweet['negative']
                if tweet['negative'] != 0:
                    ratio = tweet['positive'] / tweet['negative']
                    avg_ratio = avg_ratio + ratio
                else:
                    avg_ratio= avg_ratio +1
                length = len(responses)
                avg_responses = avg_responses + length

            if number_tweets != 0:

                avg_ratio = avg_ratio / number_tweets # Average ratio
                total_responses = avg_responses
                avg_responses = avg_responses / number_tweets # Average responses
                pos_avg = pos_total / number_tweets
                neg_avg = neg_total / number_tweets

            values = [total_responses,pos_total,neg_total]
            objects = ["Total Responses", "Total Pos", "Total Neg"]

            title = "Total values for {}".format(person)

            if date:
                title = "Total values for {} on {}".format(person,str(date))

            self.print_plot(objects,values,title,person)

            values = [number_tweets,avg_responses,pos_avg,neg_avg]
            objects = ["Number Tweets","Avg Responses", "Avg Pos", "Avg Neg"]
            title = "Average values for {}".format(person)

            if date:
                title = "Average values for {} on {}".format(person,str(date))
            self.print_plot(objects,values,title,person)

            #TODO automatic dictionary creation?
            return {"total_responses":total_responses,"pos_total":pos_total,"neg_total":neg_total,
                    "number_tweets":number_tweets,"avg_responses":avg_responses,"pos_avg":pos_avg,
                    "neg_avg":neg_avg,"avg_ratio":avg_ratio}

        else:
            print("No Data for user {}!".format(person))

    def get_responses_for_tweet(self,tweet_id):

        res = self.query(self._sql_get_responses,[str(tweet_id)])
        return res

    def get_responses_for_hashtag(self,htag_id):

        res = self.query(self._sql_get_responses_htag,[str(htag_id)])
        return res

    def get_statistics_by_hashtag(self,hashtag,date=None):
        """

        :param hashtag:
        :return:
        """

        hashtag_tweet = self.get_hashtag(hashtag)[0]
        if hashtag_tweet:
            responses = self.get_responses_for_hashtag(hashtag_tweet['htag_id'])

            if date:
                responses = self.query(self._sql_get_responses_htag_date,[hashtag_tweet['htag_id'],date])

            pos_total = 0.0
            neg_total = 0.0

            total_responses = len(responses)

            for response in responses:
                if response['sentiment'] == "positive":
                    pos_total = pos_total + 1.0
                if response['sentiment'] == 'negative':
                    neg_total = neg_total + 1.0


            values = [total_responses,pos_total,neg_total]
            objects = ["Total Tweets", "Total Pos", "Total Neg"]

            title = "Total values for #{}".format(hashtag)

            if date:
                title = "Total values for #{} on {}".format(hashtag,str(date))

            self.print_plot(objects,values,title,hashtag)


    def get_most_negative_tweet(self,count=1):
        """

        :param count:
        :return:
        """
        qry = u"select * from tweets where (positive/negative)= (select min(positive/negative) " \
              u"from tweets where positive>0 and negative>0 and twitter_tweet_id != '0' );"

        res = self.query_one(qry)

        print("## the most negative tweet is: ")
        print(res['tweet'])

        person = self.query_one(self._sql_get_acc_name,[res['acc_id']])

        print("from {} with {} positive and {} negative responses. \n".format(person['acc_name'],res['positive'],res['negative']) )

    def get_most_negative_tweet_by_person(self,person,count=1):
        """
        :param count:
        :return:
        """

        id = self.query_one(self._sql_get_acc_id,[person])

        id = id['acc_id']

        qry = u"select * from tweets where (positive/negative)= (select min(positive/negative) " \
              u"from tweets where positive>0 and negative>0 and twitter_tweet_id != '0' and acc_id=%s);"

        res = self.query(qry,[id])

        if res:
            for r in res:
                if r['acc_id'] != id: #TODO why does that happen? Error in SQL statement?
                    continue
                print("## the most negative tweet from {} is: ".format(person))
                print(r['tweet'])

                person = self.query_one(self._sql_get_acc_name,[r['acc_id']])

                print("with {} positive and {} negative responses. \n".format(r['positive'],r['negative']))


    def get_most_positive_tweet(self,count=1):
        """

        :param count:
        :return:
        """
        qry = u"select * from tweets where (positive/negative)= (select max(positive/negative) " \
              u"from tweets where positive>0 and negative>0 and twitter_tweet_id != '0' );"

        res = self.query_one(qry)

        print("## the most positive tweet is: ")
        print(res['tweet'])

        person = self.query_one(self._sql_get_acc_name,[res['acc_id']])

        print("from {} with {} positive and {} negative responses. \n".format(person['acc_name'],res['positive'],res['negative']) )


    def get_most_positive_tweet_by_person(self,person,count=1):
        """
        :param count:
        :return:
        """
        id = self.query_one(self._sql_get_acc_id,[person])

        id = id['acc_id']

        qry = u"select * from tweets where (positive/negative)= (select max(positive/negative) " \
              u"from tweets where acc_id=%s and positive>0 and negative>0 and twitter_tweet_id != '0');"

        res = self.query(qry,[id])
        if res:
            for r in res:
                if r['acc_id'] != id: #TODO why does that happen? Error in SQL?
                    continue
                print("## the most positive tweet from {} is: ".format(person))
                print(r['tweet'])

                person = self.query_one(self._sql_get_acc_name,[r['acc_id']])

                print("with {} positive and {} negative responses. \n".format(r['positive'],r['negative']))


def handle_args(dir_path):
  ##Set logging level from commandline
    parser = argparse.ArgumentParser()
    parser.add_argument(
        '-d', '--debug',
        help="print debug statements to logfile",
        action="store_const", dest="loglevel", const=logging.DEBUG,
        default=logging.WARNING,
    )
    parser.add_argument(
        '-v', '--verbose',
        help="verbose output to logfile",
        action="store_const", dest="loglevel", const=logging.INFO, default=logging.WARNING
    )

    args = parser.parse_args()

    log = logging.getLogger("log_main_stream")

    logging.basicConfig(
        format="%(asctime)-15s [%(threadName)-12.12s] [%(levelname)-5.5s]  %(message)s")
    log.addHandler(logging.FileHandler(os.path.join(dir_path,"start_analyser.log")))
    log.addHandler(logging.StreamHandler(sys.stdout))
    log.setLevel(args.loglevel)

    return args
    ## Mentions directly at person,hashtag etc.. save only the analysis
    ## Replies to tweet, also save original tweet


def main():

    # Get Configuration data from config file

    dir_path = os.path.dirname(os.path.realpath(__file__))
    config = ConfigParser.ConfigParser()
    dir_path , _ = os.path.split(dir_path)
    dir_path , _ = os.path.split(dir_path)
    config.read(os.path.join(dir_path,"config.ini"))

    handle_args(dir_path)

    statistics = Statistics(config.get("MySQL","HOST"),config.get("MySQL","USER_NAME"),config.get("MySQL","USER_PWD"), config.get("MySQL","DB_NAME"))

    print("Run Statistics Tool..")
    print("Figures will be saved in {}/figures/".format(dir_path))

    statistics.get_overall_statistics()

    print("############### \n ## Most Negative Tweets ## ## ## ## ## ## ## ## ## \n")

    statistics.get_most_negative_tweet()

    persons = statistics.get_all_person_names()

    for person in persons:
        statistics.get_most_negative_tweet_by_person(person)

    print("############### \n ## Most Positive Tweets ## ## ## ## ## ## ## ## ## \n")
    statistics.get_most_positive_tweet()
    for person in persons:
        statistics.get_most_positive_tweet_by_person(person)


if __name__ == '__main__':

    main()