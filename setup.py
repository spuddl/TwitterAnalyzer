#!/usr/bin/env python

from setuptools import setup

setup(
    name="Twitter Analyser",
    description="Twitter Analysis tool to save tweets in database and retrieve the sentiment",
    version="0.2",
    author="spuddl",
    author_email="spuddl@gmx.net",
    url="TODO gitlab",
    license="GNU GPLv3",
    packages=[
        "twitter_analyser", "twitter_analyser.analyser", "twitter_analyser.statistics"
    ],
    install_requires=[
        "tweepy", "pid", "textblob", "MySQL-python","matplotlib"
    ],
    classifiers=[
        "Development Status :: 4 - Beta",
        "Environment :: Console",
        "Intended Audience :: Server",
        "License :: OSI Approved :: GNU General Public License v3 or later (GPLv3+)",
        "Natural Language :: English",
        "Operating System :: OS Independent",
        "Programming Language :: Python :: 2",
        "Programming Language :: Python :: 2.7",
    ]
)
